﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    private Rigidbody Player;

    private float horizontal_movement;
    private float vertical_movement;

    private bool isGrounded;

    public float speed;
    public float jumpStrength;
    private float verticalVelocity;
    private float gravity = 14.0f;
    private float jumpForce = 10.0f;


    // Use this for initialization
    void Start () {
        Player = GetComponent<Rigidbody>();
        print(this.transform.position.y);
        
    }
	
	// Update is called once per frame
	void Update () {
        horizontal_movement = Input.GetAxis("Horizontal");
        vertical_movement = Input.GetAxis("Vertical");
        float mov_y = 0;
        float mov_x = speed * horizontal_movement * Time.deltaTime;


        //print(Player.velocity.y);
        //print(Input.GetKeyDown("space"));
        //print(hit.gameObject.tag);
        if ( isGrounded )
        {

            if ( Input.GetKeyDown("space") )
            {
                print("Jump! /n jumpStrength= " + jumpStrength);
                Player.AddForce(0, jumpStrength, 0);//@TODO this is kind of a buggy jump 
            }

            //Right now only move if the player is grounded
            transform.Translate(mov_x, mov_y, 0f);
        }


        

    }

    //make sure u replace "floor" with your gameobject name.on which player is standing
    void OnCollisionEnter(Collision theCollision)
    {
        print("Collision Enter method:");
        print("Point of contact: " + theCollision.gameObject.name);
        if (theCollision.gameObject.name == "Ground")
        {
            isGrounded = true;
        }
    }

    //consider when character is jumping .. it will exit collision.
    void OnCollisionExit(Collision theCollision)
    {
        print("Collision Exit method:");
        print("Point of contact: " + theCollision.gameObject.name);
        if (theCollision.gameObject.name == "Ground")
        {
            isGrounded = false;
        }
    }
}
