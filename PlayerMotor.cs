﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMotor : MonoBehaviour {

    private CharacterController controller;

    private float verticalVelocity;
    private float gravity = 0f;
    private float jumpForce = 10.0f;

	void Start () {
        controller = GetComponent<CharacterController>();
	}
	
	void Update () {

        if (controller.isGrounded)
        {
            verticalVelocity = -gravity * Time.deltaTime;
            if (Input.GetKeyDown(KeyCode.Space))
            {
                verticalVelocity = jumpForce;
            }
        } else
        {
            verticalVelocity -= gravity * Time.deltaTime;
        }

        Vector2 moveVector = new Vector2(0, verticalVelocity);

        moveVector.x = Input.GetAxis("Horizontal") * 5.0f;
        moveVector.y = verticalVelocity;
       // moveVector.z = Input.GetAxis("Vertical") * 5.0f;
        controller.Move(moveVector * Time.deltaTime);

    }
}
