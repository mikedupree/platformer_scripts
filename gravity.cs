﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gravity : MonoBehaviour
{
    //#region Fields
    //public AnimationClip idle;
    //public AnimationClip walk;
    //public AnimationClip run;
    //public AnimationClip jump_pose;

    private Vector3 moveDirection = Vector3.zero;
    private Vector3 rotDirection = Vector3.zero;

    private CharacterController charController;

    private float fallVelocity = -9.8f;
    private float moveSpeed = 300f;
    private float jumpSpeed = 250f;
    private float jumpHeight;
    private float toMove;
    private float rotSpeed = 360;

    private bool reachedApex = false;
    private bool jumped = false;

    private enum CharState
    {
        idle,
        walking,
        running,
        jumping
    }
    private CharState charState;


    #region Functions
    void Start()
    {
       // animation.Stop();
       // animation.wrapMode = WrapMode.Loop;
        charController = GetComponent<CharacterController>(); ;

    }

    void FixedUpdate()
    {
        if (charController.isGrounded)
        {
            if (Mathf.Abs(Input.GetAxis("Horizontal")) != 0 || Mathf.Abs(Input.GetAxis("Vertical")) != 0 || Input.GetButton("Jump"))
            {
                toMove = moveSpeed * Time.deltaTime;
                jumpHeight = jumpSpeed * Time.deltaTime;
                rotDirection = new Vector3(0, Input.GetAxis("Horizontal") * Time.deltaTime * rotSpeed, 0);
                transform.Rotate(rotDirection);
                moveDirection = new Vector3(0, 0, Input.GetAxis("Vertical"));
                moveDirection = transform.TransformDirection(moveDirection);
                moveDirection *= toMove;
                if (Input.GetButton("Jump"))
                {
                    jumped = true;
                    moveDirection.y = jumpHeight;
                    charState = CharState.jumping;
                }
                else
                {
                    if (!jumped)
                        charState = CharState.walking;
                }
            }
            else
            {
                if (!jumped)
                    charState = CharState.idle;
                moveDirection = new Vector3(0, 0, 0);
            }
        }


        ApplyGravity();

        charController.Move(moveDirection * (Time.deltaTime));

        //Animation
        //Animate();
    }

    void ApplyGravity()
    {
        if (!charController.isGrounded)
        {
            moveDirection.y += fallVelocity * Time.deltaTime;

            if (moveDirection.y > 0f)
            {
                reachedApex = false;

            }
            else
            {
                reachedApex = true;

            }
        }
        else
            jumped = false;
    }

    //void Animate()
    //{
    //    if (charState == CharState.jumping)
    //    {
    //        if (!reachedApex)
    //        {
    //            //play forward jumping animations
    //            animation["jump_pose"].speed = .3f;
    //            animation.CrossFade("jump_pose");
    //        }
    //        else
    //        {
    //            //play reverse jumping animations//aka falling
    //            animation["jump_pose"].speed = -.195f;
    //            animation.CrossFade("jump_pose");

    //        }
    //    }
    //    else if (charState == CharState.walking)
    //    {
    //        animation.CrossFade("walk");
    //    }
    //    else if (charState == CharState.idle)
    //    {
    //        animation.CrossFade("idle");
    //    }
    //}
    #endregion
}